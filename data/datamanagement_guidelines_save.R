################################################################################
######################### TD 2 DATA MANAGEMENT #################################
################################################################################

# Septembre 2022

# nouveau RProject à enregistrer dans le même dossier que les données
# + ouvrir/modifier ce script (enregistré dans le même dossier également) => OK
################################################################################

# Import library
library(readxl)
library(dplyr)
library(lubridate)
library(stringr)

## Import data
source("_path.R")

pop_insee = read.csv2(file.path(path_import, "BTT_TD_POP1B_2017.csv"))
deces_insee = read.csv2(file.path(path_import, "deces-2017.csv"))
immig_insee = read_xlsx(file.path(path_import, "BTX_TD_IMG1A_2017.xlsx"))
profes_insee  =  read_xlsx(file.path(path_import, "TCRD_005.xlsx"), skip = 2)
#fct skip = saut de ligne

###################Vérification des données#####################################

str(pop_insee)
head(pop_insee)

str(deces_insee)
head(deces_insee)

str(immig_insee)
head(deces_insee)

str(profes_insee)
head(deces_insee)

################################################################################
##################### DATA MANAGEMENT ##########################################
################################################################################
##################### Fichier pop_insee ########################################

# noms de colonnes upper to lower
colnames(pop_insee) = tolower(colnames(pop_insee))

# conversion chr(nb) to int(nb)
pop_insee$nb = as.integer(pop_insee$nb)

# Création dataframe pop_totale/département avec H/F/age moyen
#filtrage par département et non arrondissement
age_pop_departement = pop_insee %>%
  filter(nivgeo != "ARM") %>% 
  
  #récupération 2 premiers chr du départe
  #somme d'age population/département
  mutate(
    departement = substr(codgeo, 1, 2), 
    somme_age = aged100 * nb 
    ) %>% 
  
  #regroupement par département   
  filter(departement != "") %>%  
  group_by(departement) %>% 
  
  #na.rm : retrait des valeurs manquantes    
  summarise(
    nb_habitants = round(sum(nb, na.rm=TRUE)),
    nb_hommes = round(sum(nb[sexe == 1], na.rm = TRUE)),
    nb_femmes = round(sum(nb[sexe == 2], na.rm = TRUE)),
    somme_age = round(sum(somme_age, na.rm = TRUE)),
    ) %>% 

  # taux_hommes_vivant =     
  mutate(
    age_moyen_vivant = round((somme_age / nb_habitants), 2)
  )

##################### Fichier deces_insee######################################

#conversion date naissance/deces 
deces_insee$datenaiss = ymd(deces_insee$datenaiss)
deces_insee$datedeces = ymd(deces_insee$datedeces)

# Création data_frame
deces_pop_depart = deces_insee %>%
  select(sexe, datenaiss, datedeces, paysnaiss, lieudeces) %>% #tri des infos
  dplyr::filter(paysnaiss == "") %>% #retrait des pops != Fr

  mutate(
    lieudeces = substr(lieudeces, 1, 2), #renommer départements
    age = round(difftime(datedeces, datenaiss, units = "days") / 365),
    #age au moment du décès
    age_ans = str_replace(age, pattern = "days", replacement = "years"),
    #pas d'autres solutions pour transformer en années
    age_ans = as.numeric(age_ans),
    ) %>%
  
  dplyr::filter(lieudeces < 98, lieudeces != "") %>% #sélection hors DROM/COM
  dplyr::filter(age_ans != "", age_ans != 0, age_ans < 100) %>% 
  #clean pop == 0 et > 100 ans
  select(sexe, lieudeces, age_ans) %>% #nouvelle sélection pour frame finale
  group_by(lieudeces) %>%
  
  summarise(
    nb_hommes = round(sum(sexe == 1)),
    nb_femmes = round(sum(sexe == 2)),
    nb_habitants = nb_hommes + nb_femmes,
    somme_age = round(sum(age_ans)) 
    ) %>%
  #difficulté d'obtention moyenne cohérente > 75a
  mutate(
    age_moyen_deces = round((somme_age / nb_habitants), 2)
  )

#colonne lieudeces renommé pour merge()
colnames(deces_pop_depart)[1] = c("departement")

##################### Fichier immig_insee#######################################

  # noms de colonnes upper to lower
colnames(immig_insee) = tolower(colnames(immig_insee)) 

#création nouvelle dataframe
imig = immig_insee %>%
  
  # résolution d'erreur en amont de na, filtre de fausses données
  replace(is.na(0), 0.0) %>%
  filter(codgeo != is.na(0)) %>%

  #arithmétique simple
  mutate(
    departement = substr(codgeo, 1, 2),
    age400_immi1 = immig_insee$age400_immi1_sexe1+immig_insee$age400_immi1_sexe2,
    age400_immi2 = immig_insee$age400_immi2_sexe1+immig_insee$age400_immi2_sexe2,
    age415_immi1 = immig_insee$age415_immi1_sexe1+immig_insee$age415_immi1_sexe2,
    age415_immi2 = immig_insee$age415_immi2_sexe1+immig_insee$age415_immi2_sexe2,
    age425_immi1 = immig_insee$age425_immi1_sexe1+immig_insee$age425_immi1_sexe2,
    age425_immi2 = immig_insee$age425_immi2_sexe1+immig_insee$age425_immi2_sexe2,
    age455_immi1 = immig_insee$age455_immi1_sexe1+immig_insee$age455_immi1_sexe2,
    age455_immi2 = immig_insee$age455_immi2_sexe1+immig_insee$age455_immi2_sexe2,
  ) %>%

  #tri des éléments désirés
  select(departement, age400_immi1:age455_immi2) %>%

  #arithmétique sur le type de population  
  mutate(
    immig_1 = age400_immi1 + age415_immi1 + age425_immi1 + age455_immi1,
    immig_2 = age400_immi2 + age415_immi2 + age425_immi2 + age455_immi2
  ) %>%
  
  # regroupement par departement %>%  
  group_by(departement) %>%   

  # addition de la pop. imig/na imig  
summarise(
    immig_1_dep = round(sum(immig_1, na.rm = TRUE)),
    immig_2_dep = round(sum(immig_2, na.rm = TRUE)),
  ) %>%
  
  #calcul du taux d'immigration par département
  mutate(
    taux_img = round((immig_1_dep * 100 / immig_2_dep) , 2)
  )

##################### Fichier profes_insee######################################

#renomme colonnes
dataprofes_insee = profes_insee
colnames(dataprofes_insee) = c("departement", "ville", "taux_agriculteurs_exploitants", 
                             "taux_artisans_commerçants_chef_entreprises", "taux_cadres",
                             "taux_profs_intermediaires", "taux_employés", "taux_ouvriers",
                             "taux_retraités", "taux_sans_act_pro") 

#tri departement hors DROM/COM /lignes commentaires INSEE
dataprofes_insee = dataprofes_insee %>%
  dplyr::filter(departement < 97, departement != "") %>%
  select(departement, taux_agriculteurs_exploitants:taux_sans_act_pro)


################################################################################

##################### Compilation nouveau fichier###############################

compil_données1 = merge(age_pop_departement,deces_pop_depart,  by = "departement")
compil_données2 = merge(imig, dataprofes_insee, by = "departement")
compil_données_fin = merge(compil_données1, compil_données2, by = "departement")


compil_données_fin = compil_données_fin %>%

  #calcul final du taux d'hommes vivants
  mutate(
    taux_hommes_vivants = round(nb_hommes.x * 100 / (nb_hommes.x + nb_hommes.y), 2)
  ) %>%
  
  select(departement, taux_hommes_vivants, age_moyen_vivant, age_moyen_deces,
         taux_img, taux_agriculteurs_exploitants:taux_sans_act_pro)

  #écriture sur fichier d'export 
write.csv2(compil_données_fin, file = file.path(path_export,"R_TD1_export.csv"), row.names = FALSE)
